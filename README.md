# lesspassmenu

## What is this?

This is a helper script to use <https://www.lesspass.com>

## Install

Requirements: bemenu, dotool, noto-fonts-emoji and lesspass

Copy `lesspass-daemon.sh`, `fingerprint.py` and `fingerprint.py` somewhere in your path, for example `$HOME/.local/bin/`.
You can create a shortcut to run `lesspassmenu.sh`.

Add yourself to the `input` group:

```sh
sudo groupadd -f input
sudo usermod -a -G input $USER
```

finally relogin.

## Usage

Run `lesspass-daemon.sh`, then you can run `lesspassmenu.sh` every time you need a password.

Create a strong master password, for example using `xkcdpass`, `diceware` (you can use an offline copy of <https://www.rempe.us/diceware/#eff> )

To create a login for the page `example.com`

1. Run `lesspass.sh`.
2. In `URL Login` input: `example.com`
3. In `Fields` input `username !some-word`
4. Type your master password

Your username and your generated password will be automatically typed.

The password is derived from the site and "!some-word". You can change `!some-word` to anything else starting with "!" and your password will change. This allows changing the generated password without changing your master password. 

The master password is stored in memory, until you type "Control+C" in the input box of bemenu.

## Save Logins

You can save the logins file, which is stored in `$HOME/.cache/bemenu-lesspass/items`

It will contain 1 line for item (most used items on top). For example:

```
example.org username !some-word
```
