#! /bin/sh
set -f # Disable globbing

# DEFAULTS
counter=1
password_length=35
options="-luds" #lowercase, uppercase, digits, special characters

# wayland or x11?
SESSION=$(loginctl show-session "$(loginctl | grep "$(whoami)" | awk '{print $1}')" -p Type | cut -d '=' -f 2)

if [ "$SESSION" = wayland ]; then
    # Right now only works in sway or sxmo
    alias focused-window="swaymsg -t get_tree | jq 'recurse(.nodes[]) | select(.nodes[].focused == true).app_id'"
else
    alias focused-window='xdotool getwindowfocus getwindowname'
fi

# create cache and make sure it is not empty.
cache_dir="${XDG_CACHE_HOME:-$HOME/.cache}/bemenu-lesspass"
cache="$cache_dir/items"
mkdir -p "$cache_dir"
test -f "$cache" || echo " " > "$cache"

handler() {
  # select / add item and login (format: URL LOGIN !PASSWORD_SEED)
  url_items="$(bemenu < "$cache" -p "URL LOGIN: " || exit)"
  # if only URL is entered, request login.
  # shellcheck disable=SC2086
  number_of_inputs=$(IFS=' '; set -f -- $url_items; echo $#)
  if [ "$number_of_inputs" -eq 1 ]; then
    url=$url_items
    items="$(grep "$url_items" "$cache" | cut -d ' ' -f 2- -s | bemenu -p 'Fields for '"$url_items"': ')" || exit
    url_items="$url $items"
  else
    url=$(echo "$url_items" | cut -d ' ' -f 1 -s)
    items=$(echo "$url_items" | cut -d ' ' -f 2- -s)
  fi

  answer=no
  while [ "$answer" = no ]
  do
    master_password=${master_password:-$(echo | bemenu -xp "Master Password: ") || exit}
    fingerprint="$(echo "$master_password" | fingerprint.py)"
    answer=$(echo "yes
no" | bemenu -p "$fingerprint")
    if [ "$answer" = no ]; then
      unset master_password
    fi
  done

  # move item and login to top / add them
  sed -i "/${url_items}/d" "$cache" #remove it
  sed -i "1i ${url_items}" "$cache" #insert it on line 1 (if cache is not an empty file)

  # Type everything for you.
  case $(focused-window) in
    'Alacritty')
      separator="enter"
      ;;
    *)
      separator="tab"
  esac

  for it in $items
  do
    case $it in
    '!'*)
      field=$(echo "$it" | cut -c 2-)
      FIELD=$(LESSPASS_MASTER_PASSWORD=$master_password lesspass "${url}" "${field}" $options -L $password_length -C $counter -p)
      ;;
    *)
      FIELD=$it
      ;;
    esac

    echo "type $FIELD" | dotool
    echo "key $separator" | dotool
  done
}

trap handler USR1

while :
do
    sleep 1
done
