#! /bin/sh

# If the daemon is not started, start it. It will recall the master pasword.

if [ -z "$(pgrep lesspass-daemon)" ]; then
    lesspass-daemon.sh &
    echo "lesspass-daemon.sh started"
    sleep 1
fi

# Send signal to daemon
kill -USR1 "$(pgrep lesspass-daemon)"
